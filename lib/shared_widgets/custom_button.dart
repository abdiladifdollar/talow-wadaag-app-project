import 'package:flutter/material.dart';

Widget customButton({required Function onPressed, required String label}) {
  return SizedBox(
    width: 200.0, // Set your desired width
    height: 50.0, // Set your desired height
    child: ElevatedButton(
      onPressed: onPressed(),
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(const Color(0xFF52CC99))),
      child: Text(
        label,
        style: const TextStyle(color: Colors.white),
      ),
    ),
  );
}
