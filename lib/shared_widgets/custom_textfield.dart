// ignore: file_names
import 'package:flutter/material.dart';

Widget customTextField(
    {required TextEditingController controller,
    required String label,
    String? hint}) {
  return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
    Text(
      label,
      style: const TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
      textAlign: TextAlign.left,
    ),
    const SizedBox(
      height: 10,
    ),
    TextField(
      controller: controller,
      decoration: InputDecoration(
        hintText: hint,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    ),
  ]);
}
