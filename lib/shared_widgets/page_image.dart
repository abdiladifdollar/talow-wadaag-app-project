import 'package:flutter/material.dart';

Widget pageImage({required String imageLink}) {
  return Container(
    height: 250,
    decoration: const BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.white,
      boxShadow: [
        BoxShadow(color: Colors.green, spreadRadius: 3),
      ],
    ),
    child: Image.asset(imageLink),
  );
}

Widget pageLabel({required String desc}) {
  return Text(
    desc,
    textAlign: TextAlign.center,
    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
  );
}
