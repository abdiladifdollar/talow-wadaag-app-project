import 'package:flutter/material.dart';
import 'package:talowadaag_app/shared_widgets/export_widgets.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({super.key});

  @override
  State<ChangePasswordScreen> createState() => ChangePasswordScreenState();
}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    var pass = TextEditingController();
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: const Text('Create New Password'),
      ),
      body: Padding(
          padding: EdgeInsets.only(
              top: screenWidth * 0.1,
              right: screenHeight * 0.02,
              left: screenHeight * 0.02),
          child: Center(
            child: Column(
              children: [
                pageImage(
                    imageLink: '../asset/images/changePassScreenImage.png'),
                const SizedBox(
                  height: 60,
                ),
                pageLabel(
                    desc:
                        'Your new password must be different from your previous used password'),
                const SizedBox(
                  height: 50,
                ),
                customTextField(
                    controller: pass,
                    label: 'New Password',
                    hint: 'Enter new password'),
                const SizedBox(
                  height: 30,
                ),
                customTextField(
                    controller: pass,
                    label: 'Confirm Password',
                    hint: 'Confirm new password'),
                const SizedBox(
                  height: 50,
                ),
                customButton(onPressed: () {}, label: 'Submit')
              ],
            ),
          )),
    ));
  }
}
