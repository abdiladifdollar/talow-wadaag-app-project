import 'package:flutter/material.dart';
import 'package:talowadaag_app/shared_widgets/export_widgets.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({super.key});

  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    var otpValue = TextEditingController();
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Verify Page'),
        ),
        body: Padding(
          padding: EdgeInsets.only(
              top: screenWidth * 0.1,
              right: screenHeight * 0.02,
              left: screenHeight * 0.02),
          child: Column(
            children: [
              pageImage(imageLink: '../asset/images/verifyScreenImage.png'),
              const SizedBox(
                height: 60,
              ),
              const Text(
                'Please Enter the 4 digit code sent to your email \n (demo@gmail.com)',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
              const SizedBox(
                height: 60,
              ),
              customTextField(controller: otpValue, label: 'Enter Code'),
              const SizedBox(
                height: 60,
              ),
              Center(child: customButton(onPressed: () {}, label: 'Verify')),
            ],
          ),
        ),
      ),
    );
  }
}
